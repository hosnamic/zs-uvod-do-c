# Co to je?

Jedna se o verejny repozitar, kde je hlavnim *artiklem* soubor zbytecneSlozityUvodDoJazykaC.md - coz by mel byt jakysi doplnujici material, ke studiu predmetu BI-PA1. Dokument je psany jako `markdown`, takze je citelny ve stazene podobe a zaroven tady na GitLabu je do jiste miry formatovany.

* [(zbytecne slozity) uvod do jazyka C pro neprogramatory](https://gitlab.fit.cvut.cz/sadekkry/zs-uvod-do-c/blob/master/zbytecneSlozityUvodDoJazykaC.md) [hotovo]
* Co jsou to ty programovani [planuje se]

## FAQ
* **Proc nepouzivas diakritiku?**
* Protoze jsem linej. Klidne mi to zreviduj a posli *normalni* verzi.
* **Pro koho je tohle urceny?**
* Pro kohokoliv. Ale pisu to s ohledem na lidi, co v zivote neprogramovali a prijdou si lehce ztraceni na BI-PA1.
* **Nelibi se mi / nechapu / prijde mi zbytecny / ... to a to**
* Viz errata
* **Programovat umim a ocividne lepe nez ty, nechces pomoc?**
* Jasna vec. Proste vytvor commit a pushni to sem. Nebo mi napis na mail. Cokoliv

## Errata
Pokud narazite na chybu (vecnou, gramatickou) nebo si myslite, ze by neco melo byt formulovano jinak, vytvore novou [issue](https://gitlab.fit.cvut.cz/sadekkry/zs-uvod-do-c/issues). 

## Changelog
3. [17-10-2017 09:10] Castecne vyresena issue #1; diky za zpetnou vazbu Davidovi a Klare
2. [15-10-2017 22:06] Opravena gramatika, dopsana kapitola **4** a **5**. Naplanovano vytvorit *Co jsou to ty programovani*.
1. [15-10-2017 17:50] Vytvorena prvni verze, chybi nejake kapitoly.