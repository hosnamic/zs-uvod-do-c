# (zbytecne slozity) uvod do jazyka C pro neprogramatory

**Kod nedisponuje temi nejlepsimi navyky a nektere veci by vam cvici omlatili o hlavu. Tento dokument berte jako *FIT-wiki* material.** Jeho primarnim cilem je vysvetlit nalezitosti uvedeneho programu; to ze uvedeny program je nebo neni prasarna, je vec jina.

## 1. Uvod
1. Co
  * Cilem bude vytvorit program, ktery se uzivatele zepta na vek a jmeno. Vystupem bude nejaky text, na zaklade vstupnich udaju.
2. Proc
  * Program je navrzeny tak, aby zahrnoval vsechny nalezitosti, ktere je potreba pochopit do zacatku.
3. Jak
  * Kazdy radek kodu, prezentujici novy *prvek*, bude doplnen o teoreticky vyklad.

## 2. Vysledny kod

```c
#include <stdio.h>

int vek;

int main(int argc, char *argv[])
{
    char jmeno[32] = {'\0'};

    printf("Zadejte vas vek:\n");
    if (scanf("%d", &vek) == 1)
    {
        printf("Ted tvoje jmeno:\n");
        scanf("%31s", jmeno);

        int i;
        for (i = 0; i < vek; ++i)
        {
            // TODO
        }

        printf("%s (vek %d) uspesne NEabsolvuje BI-PA1!\n", jmeno, vek);
    }
    else
        printf("Tohle neni vek, borce.\n");
    
    return 0;
}
```

## 3. Rozbor kodu
### 3.1 `#include <...>`
Tohle neni C! Jedna se o instrukci pro *preprocesor*. Ten nejakym zpusobem upravi zdrojovy kod, ktery jste napsali. V tomto pripade mu rikate, at vezme obsah souboru ve spicatych zavorkach a prekopiruje ho do vaseho kodu. `ctrl+c` `ctrl+v`. Onen soubor je taky zdrojovy kod. Viz [stdio.h](https://www.gnu.org/software/m68hc11/examples/stdio_8h-source.html).

Stejnym zpusobem muzete svuj kod roztahat do nekolika souboru a potom je naincludovat. V tom pripade nepouzijete `<...>`, ale `"..."` (rozdil je v tom, kde preprocesor hleda ony soubory; prvni varianta ho smeruje ke knihovnam, ta druha do aktualniho adresare).

### 3.2 `int vek;`
Tohle uz C je. A konkretne jde o *deklaraci promenne*. Povsimnete si, ze do ni neprirazujeme zadnou hodnotu. (Ale i tak v ni bude ulozena nula. Proc? Jedna se o tzv. **globalni promennou** - ta se nenachazi uvnitr zadne funkce a tudiz k ni mate pristup v celem zbytku kodu. Zaroven je zkratka definovano, ze globalni promenne se inicializuji na hodnotu nula, neni-li dano jinak.)
1. `int`
  * Nejprve se uvadi jeji **datovy typ**.
2. `vek`
  * Pak nasleduje jmeno promenne, pod kterym se na ni budete odkazovat. Jedna se o **identifikator**.
3. `;`
  * V C plati, ze *co radek, to prikaz*. Ovsem C nechape odentrovani jako konec radku. Misto toho je potreba pouzit strednik. Zaroven z toho plyne, ze pokud budete cunata, muzete cely kod napsat na jediny radek. Proste; bude; vsude; strednik;

### 3.3 `int main(int argc, char *argv[])`
No a tady mame **deklaraci funkce**. Je proste dane, ze kdyz spustite program, operacni system z nej zavola funkci, ktera se jmenuje `main`. Jedna se o entry point vaseho programu.

*Proc zrovna `int` jako navratova hodnota je v sekci 4.1* a *co je to * argv[] je v sekci 4.2.*
1. `int`
  * Obdobne jako u promenne, opet definujeme datovy typ. V tomto pripade se jedna o datovy typ hodnoty, kterou vam funkce vrati. (Pokud funkce nema vracet nic, pouzijete jako typ `void`.)
2. `main`
  * Nasleduje identifikator; jmeno funkce.
3. `(...)`
  * Na zaver se uvadi kulate zavorky, do kterych vypisete **vstupni argumenty** oddelene carkou. Muze jich byt nula, nebo treba pet, ale kulate zavorky je potreba uvest vzdy - tim je dany rozdil mezi deklaraci funkce a promennych. u jednotlivych argumentu opet plati pravidlo *datovy_typ nazev*.

### 3.4 `{...}`
Slozene zavorky definuji **blok kodu**. V tomto pripade je to blok kodu, ktery se ma vykonat pri zavolani funkce `main`.

### 3.5 `char jmeno[32] = {'\n'};`
Opet definice promenne. Tentokrat jde o promennou **lokalni**; existuje pouze v prislusnem bloku kodu (zde je onim blokem *telo funkce `main`*). Zaroven do ni za `=` rovnou prirazujeme i hodnotu.
Zajimavejsi je ona cast `[32]`. Ta rika, ze vytvarime cele **pole** promennych.

#### 3.5.1 Pole
Kdyz deklarujete promennou, nekde v pameti na adrese razdva se zarezervuje pocet bytu, ktery odpovida velikosti datoveho typu. Pro `char` to je jeden byte, u `float` jsou ctyri (viz fce `sizeof()`).

No a pole je skoro to same - jen s tim rozdilem, ze na adrese razdva zarezevuje pocet bytu, odpovidajici velikosti datoveho typu **krat cislo v hranatych zavorkach**. V tomto pripade tedy chceme `sizeof(char) * 32` bytu.

#### 3.5.2 Textove retezce
C je hloupy jazyk. Zna pouze datove typy pro cisla. Cela cisla, zaporna cisla, destinna cisla... Ale textovy retezec ulozit neumi. Z toho duvodu si pomahame pres pole typu `char`.

#### 3.5.2 `= {'\0'}`
To, ze mame pole o velikosti 32 bytu jeste neznamena, ze v nem bude ulozeno 32 znaku. K tomu, aby se urcilo *kde konci textovy retezec* slouzi takzvany **terminator**. V ASCII tabulce je to prvni znak, ma ciselnou hodnotu nula a zapisuje se `\0`. Vsechny standardni funkce jsou pak napsane tim stylem, ze `\0` chapou jako konec textu a zadne nasledujici byty uz nezpracovavaji.

A ty slozene zavorky zde neznaci *blok kodu*, ale spis *mnozinu*. Timto zpusobem lze zadeklarovat pole. Napriklad `int cisla[] = {1,2,3,4}` vytvori pole typu `int` a i pres to, ze jsme explicitne neuvedli jeho velikost, pocet prvku v zavorce to rika za nas.

### 3.6 `printf(...);`
Tak tady volame funkci. Jedna se o standardni funkci definovanou v `stdio.h` a slouzi k *f*formatovanemu tisknuti textu na standardni vystup.

### 3.7 `if (scanf("%d", &vek) == 1)`
`if` neni funkce, ale jednim z **klicovych slov** jazyka C. Jedna se o podminku - *"pokud je vyraz v nasledujicich kulatych zavorkach pravda, vykonej nasledujici prikaz"*. Casto tech prikazu ale chceme vykonat vic; z toho duvodu pouzijeme *blok kodu*.

`scanf(...)` je opet funkce ze `stdio.h`. Pouziva se k nacteni hodnot ze standardniho vstupu. Jeji **navratova hodnota** je cele cislo, odpovidajici poctu hodnot, ktere se nacetli. V tomto pripade nacitame jednu, takze ocekavama hodnotu jedna. *Proc je pred druhym argumentem `&` (a proc u dalsiho volani `scanf` neni) je vysvetleno v sekci 4.2.*

`==` neplni funkci prirazeni, ale *porovnani*. "Je-li hodnota vlevo stejna jako vpravo, vyraz je pravdivy."

### 3.8 `for (i = 0; i < vek; ++i)`
[-Je potreba prepsat-]. Opet zde mame *klicove slovo*, tentokrat `for` - *"proved prikaz z kulate zavorky; dokud plati podminka v kulatych zavorkach za prvnim strednikem, proved prikaz za druhym strednikem a ten, co nasleduje za kulatou zavorkou"*. Jinymi slovy - `int i = 0` je *inicializace cyklu*, `i < vek` je jeho *podminka* a `++i` rekneme *rezie*.

Vytvor promennou `i` a uloz do ni hodnotu *nula*. Je `i` mensi nez `vek`? Proved `++i` (tzn. pricti k nemu jednicku) a vykonej prikaz za kulatou zavorkou (opet *blok kodu*).

**POZOR!** `for (;;)` je zcela legalni zapis cyklu. Jelikoz zde neni zadna podminka, pobezi do nekonecna. *Vic na tema 'unik z cyklu' v sekci 4.3.*

### 3.9 `// TODO`
Obcas se hodi doplnit kod o nejake informace pro vas, jakozto programatora. K tomu slouzi **komentare**. Bud muzete pouzit komentar **jednoradkovy** - viz priklad `//`, nebo se rozepsat trochu vic, konkretne pouzit `/* viceradkovy komentar */`.

### 3.10 `else`
Dalsi klicove slovo! `else` se vzdy pouziva v kontextu podminky `if` a rika *"pokud je vyraz v kulatych zavorkach podminky nepravda, proved nasledujici prikaz"*. Zaroven zde muzete videt, ze opravdu neni nutne vkladat blok kodu, ale staci jediny prikaz.

#### 3.10.1 `if ... else if ... else ...`
Samozrejme je mozne podminky vetvit:
```c
if (vek <= 0) printf("nelzi\n");
else if (vek > 150) printf("to bys byl mrtvej\n");
else printf("ok..\n");
```
### 3.11 `return 0;`
A tady konci deklarace nasi funkce. Pokud neuvedete jako jeji *navratovy typ* `void`, **vzdy** musi na konci obsahovat *klicove slovo* `return`. A tim je mysleno na jejim *logickem konci*. Napriklad by nekde uprostred mohla byt podminka, ze kdyz uzivatel zada vek vetsi nez 300, program skonci.

### 3.12
To je vse! C je plne zakernosti, na ktere urcite behem *progtestovani* narazite. V dalsi sekci nektere z nich jeste zminim a pro ty zvedavejsi uvedu i trochu teorie, co se deje pod kapotou.

## 4 Doplneni rozboru kodu

### 4.1 Operacni system
Aby program mohl bezet pod operacnim systemem, je potreba dodrzovat urcita pravidla. Jednim z nich je deklarace funkce `main` v takove podobe, jakou zde vidite. Navratova hodnota `int` je informace pro OS. Tim mu predavate informaci o tom, jestli program udelal to, co mel. Nula znaci, ze program skoncil bez chyb. Kdyz vratite "male nenulove cislo", rikate tim systemu, ze asi neco nedopadlo jak melo. Dobre si to muzete predstavit na retezeni prikazu v shellu (viz BI-PS1) - kdyz napisete `prikaz1 && prikaz2`, chcete aby vykonal prvni prikaz a v pripade uspechu i ten druhy. No a onen uspech je definovany prave jako navratova hodnota *nula*.

Krome navratove hodnoty ma `main` i vstupni argumenty. Jejich prostrednictvim muzete ovlivnovat chovani vaseho programu. `argc` je celociselna hodnota, ktera vam rika, kolik argumentu bylo vasemu programu predano. **Vzdycky** ma hodnotu nejmene jedna. Je to proto, ze prvnim *argumentem* je chapan nazev programu. Tzn. pokud vasi aplikaci pojmenujete *karlikovaTovarnaNaKecup.exe*, bude prvnim argumentem prave tento retezec. A kde ho najdete? No od toho je `argv[]` - pole argumentu. v `argv[0]` je *karlikovaTovarnaNaKecup.exe*. Opet viz BI-PS1, nazorny priklad:
```
ls -l Dokumenty/
- argc je 3
- argv[0] je "ls"
- argv[1] je "-l"
- argv[2] je "Dokumenty/"
```
Samozrejme muzete `main` zadefinovat jako `main(int pepa, char *konkubina[])` - pak misto `argc` bude v *pepovi* ulozen pocet *konkubin*. Nedela se to z duvodu citelnosti kodu vasimi kolegy.

### 4.2 Ukazatele
**Ukazatele jsou velmi (zne)uzivanou soucasti jazyka C a jejich pochopeni je klicove. Ovsem v zacatcich muze byt jejich koncept spis matouci.** Pozdeji se k nim dostanete na cviceni; prozatim na ne muzete zapomenout. Tato sekce je vazne jen pro ty zvedavejsi.

Prokleti pro novacky, mocny nastroj pro zkusene. **Ctete na vlastni nebezpeci!** Pokud vytvorite promennou, ulozi se nekam do pameti a kdyz ji zavolate, pocitac to chape jako *"hej! Sahni do pameti na adresu xyz a podej mi n bytu"*; tech *n* bytu je urceno *datovym typem* promenne. Ale co kdyz by vas z nejakeho duvodu zajimalo, na jake adrese ta promenna lezi? Aha! A presne tady prichazi do hry **pointery**, buransky - ehm cesky - ukazatele.
```c
int *ukazatel = &promenna;
```
Tak tady si muzete vsimnout dvou specialnich znaku, ktere si C vyhradilo pro praci s ukazateli (ale zalezi na kontextu - `*` se v aritmetickem vyrazu pouziva jako nasobeni a `&` slouzi jako logicka konjunkce). `int *ukazatel` je **ukazatel na promennou typu integer** a `&promenna` je **adresa, na ktere se `promenna` nachazi**. Nic vic, nic min.

Takze co je `char *argv[]`? *Pole ukazatelu na promenne typu char*. Coze... No pro vysvetlenou se hodi mensi odbocka k polim. Ono totiz pole v Cecku neni tak docela promenna, ale prave *ukazatel*. Kdyz zadefinujete `int cisla[5]`, a pak si zkusite pomoci `printf("%p", cisla)` vytisknout obsah pole, vrati vam to jen nejaky osklivy cislo. Coz je prave adresa, na ktere ono pole zacina. Takze `char *argv[]` je vlastne pole poli. A jelikoz textovy retezec se uklada jako pole, neni tezke si domyslet, ze se to da prakticky chapat jako **pole textovych retezcu**.

No a tim padem `scanf("%d", &vek)` uz urcite chapete, ze funkci `scanf` predavate jako argument **adresu promenne `vek`**. Proc? Je to z toho duvodu, ze **funkce v C umi vratit jen jednu hodnotu**. Takze kdyz chcete pomoci `scanf` nacist hodnot nekolik, nejde to pres jeji navratovou hodnotu. Navic by byl problem s datovym typem. Jednou by musel byt `iscanf` pro cteni integeru, podruhe `fscanf` pro cteni floatu, a tak dale. Takze se udela to, ze *jako argumenty se predaji adresy, na kterych se nachazi promenne do nichz funkce ulozi zjistene hodnoty*.

### 4.3 Cykly
Krome uvedeno cyklu `for`, ktery se pouziva pro pevne dany pocet opakovani, mame k dispozici jeste `while` (resp. `do ... while`). Syntaxi maji obdobnou, ale zpravidla je pouzivame ve chvili, kdy dopredu neni znamy pocet opakovani.

#### 4.3.1 `continue` a `break`
Obcas muzete narazit na situaci, kdy je potreba cyklus ukoncit predcasne. K tomu slouzi prave *klicove slovo* `break`. Tim rikate, at se provadeny cyklus utne.

Oproti tomu `continue` preskoci pouze aktualne vykonavany beh cyklu:
```
1. for (...)
2. a = 1 + 1
3. continue <- skoc na radek jedna 
4. a = a + 1 <- to uz se nevykona
```

#### 4.3.2 `do ... while`
syntaxe je `do {telo cyklu} while (podminka)`. Z toho plyne, ze *i kdyby podminka neplatila, telo cyklu se jednou provede*!

Oproti tomu `while (podminka) {telo cyklu}` nejprve overi podminku a az na zaklade vyhodnoceni pokracuje k (ne)vykonani.

## 5 Zaver
Dalsi podklady hledejte na [edux](ehttps://edux.fit.cvut.cz/courses/BI-PA1/) a pres [google](https://www.bing.com/search?q=google). Odtud uz je to na vas. Chodte na hodiny, davejte pozor, **ptejte se** a hlavne zkousejte programovat. Teorie je pekna vec, ale naucite se to az praxi.

![xkcd](https://imgs.xkcd.com/comics/goto.png)